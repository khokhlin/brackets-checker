import sys
from .brackets_checker import check


if len(sys.argv) < 2:
    sys.exit("Text argument is required")


text = "".join(sys.argv[1:])

if check(text):
    print("All brackets are matched")
else:
    print("Some brackets unmatched")
