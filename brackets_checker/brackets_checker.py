"""Brackets checker"""


OPENED_BRACKETS = "[", "(", "{"
CLOSED_BRACKETS = "]", ")", "}"
PAIRS = {
    OPENED_BRACKETS[0]: CLOSED_BRACKETS[0],
    OPENED_BRACKETS[1]: CLOSED_BRACKETS[1],
    OPENED_BRACKETS[2]: CLOSED_BRACKETS[2],
}


def check(text: str) -> bool:
    """Checks if all brackets in the text are closed correctly.

    Successful checks
    >>> assert check("") is True
    >>> assert check("[]") is True
    >>> assert check("()[]") is True
    >>> assert check("[]()") is True
    >>> assert check("[](){}") is True
    >>> assert check("[ab]()") is True
    >>> assert check("[ab](ab)") is True
    >>> assert check("[ab](ab)ab{ab}") is True
    >>> assert check("abc") is True

    Checks with errors
    >>> assert check("[(]") is False
    >>> assert check("[(](") is False
    >>> assert check("[({](}") is False
    >>> assert check("[)](") is False
    >>> assert check("{[)](}") is False
    >>> assert check("[])") is False
    >>> assert check("[](") is False
    >>> assert check("abc[") is False
    >>> assert check("abc]") is False
    >>> assert check("[abc") is False
    >>> assert check("]abc") is False
    >>> assert check("abc}") is False
    >>> assert check("abc{") is False
    """

    opened = []
    for symbol in text:
        if symbol in OPENED_BRACKETS:
            opened.append(PAIRS[symbol])
        elif symbol in CLOSED_BRACKETS:
            try:
                match = opened.pop()
            except IndexError:
                return False
            else:
                if match != symbol:
                    return False
        else:
            continue

    if opened:  # There are some unclosed brackets
        return False
    return True
