# Brackets checker

### Usage

```sh
python -m brackets_checker text with brackets []
```

### Tests
```sh
python -m doctest brackets_checker/brackets_checker.py
```
